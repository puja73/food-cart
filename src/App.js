import React from "react";
import "./App.css";
import { Shopping } from "./components/shopping/shopping";

const App = () => {
  return (
    <div className="App">
      <Shopping />
    </div>
  );
};

export default App;
