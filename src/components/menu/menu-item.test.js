import { render, screen } from "@testing-library/react";
import { PRODUCTS } from "../products.const";
import { ShoppingContext } from "../shopping/shopping.context";
import { MenuItem } from "./menu-item";

it("should render a product image", () => {
  render(
    <ShoppingContext.Provider
      value={{
        basket: [PRODUCTS[0]],
      }}
    >
      <MenuItem product={PRODUCTS[0]} />
    </ShoppingContext.Provider>
  );

  const img = screen.getByRole("img");
  expect(img).toBeInTheDocument();
});

it("should render product name", () => {
  render(
    <ShoppingContext.Provider
      value={{
        basket: [],
      }}
    >
      <MenuItem product={PRODUCTS[0]} />
    </ShoppingContext.Provider>
  );

  const heading = screen.getByRole("heading");

  expect(heading).toBeInTheDocument();
});


it("should render product name", () => {
  render(
    <ShoppingContext.Provider
      value={{
        basket: [],
      }}
    >
      <MenuItem product={PRODUCTS[0]} />
    </ShoppingContext.Provider>
  );

  const heading = screen.getByRole("heading");

  expect(heading).toBeInTheDocument();
});
