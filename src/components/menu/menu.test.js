import { render, screen } from "@testing-library/react";
import { ShoppingContext } from "../shopping/shopping.context";
import { Menu } from "./menu";

it("should render Menu as header", () => {
  render(
    <ShoppingContext.Provider
      value={{
        basket: [],
      }}
    >
      <Menu />
    </ShoppingContext.Provider>
  );

  const header = screen.getByTestId("22");
  expect(header).toBeInTheDocument();
});

it("should render multiple menu items", () => {
  render(
    <ShoppingContext.Provider
      value={{
        basket: [],
      }}
    >
      <Menu />
    </ShoppingContext.Provider>
  );

  const listItem = screen.getAllByRole("listitem");
  expect(listItem.length).toBeGreaterThan(0);
});
