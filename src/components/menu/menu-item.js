import { useShoppingContext } from "../shopping/shopping.context";
import { truncate } from "../utils";

export const MenuItem = ({ product }) => {
  const { basket, handleAddItemToBasket } = useShoppingContext();

  const renderActionButton = product => {
    const hasProduct = basket.some(item => item.id === product.id);

    if (hasProduct) {
      return <div className="in_the_cart">In the cart</div>;
    }

    return (
      <button
        onClick={() => handleAddItemToBasket(product)}
        className="add_to_cart"
      >
        Add to cart
      </button>
    );
  };

  return (
    <>
      <div>
        <img src={product.img} alt={product.name} />
      </div>

      <div>
        <div className="product_info">
          <h2>{product.name}</h2>
          <p>
            <em>{truncate(product.description, 50)}</em>
          </p>
          <p>{`$${product.price}`}</p>
        </div>

        {renderActionButton(product)}
      </div>
    </>
  );
};
