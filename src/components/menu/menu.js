import React from "react";
import { PRODUCTS } from "../products.const";
import { MenuItem } from "./menu-item";
import "./menu.css";

export const Menu = () => {
  return (
    <div className="menu_container">
      <h1 data-testid="22">Menu</h1>
      <hr />

      <ul className="list">
        {PRODUCTS.map((product, idx) => (
          <li key={idx} className="list_item">
            <MenuItem product={product} />
          </li>
        ))}
      </ul>
    </div>
  );
};
