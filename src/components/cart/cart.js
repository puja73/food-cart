import React from "react";
import { PRODUCTS } from "../products.const";
import { useShopping } from "../shopping/shopping.hook";
import { CartItem } from "./cart-item";
import "./cart.css";

export const Cart = () => {
  const { basketItems, basketItemsLength, basketTotalPrice } = useShopping();

  const renderInCartItems = () => {
    const items = Object.keys(basketItems).map(id =>
      PRODUCTS.find(product => product.id === id)
    );

    return (
      <ul>
        {items.map(item => {
          return (
            <li key={item.id}>
              <CartItem item={item} />
            </li>
          );
        })}
      </ul>
    );
  };

  return (
    <div className="cart_container">
      <h1>Cart</h1>
      <hr />

      <div className="cart_details">
        {basketItemsLength === 0 ? (
          <h2>Your cart is currently empty.</h2>
        ) : (
          renderInCartItems()
        )}
      </div>

      <hr style={{ marginBottom: "1.2rem" }} />

      <h3 style={{ textAlign: "right" }}>
        Total: {basketTotalPrice.toFixed(2)}
      </h3>
    </div>
  );
};
