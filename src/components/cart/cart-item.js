import React from "react";
import { ActionButtons } from "../action-buttons/action-buttons";
import { useShoppingContext } from "../shopping/shopping.context";
import { useShopping } from "../shopping/shopping.hook";
import "./cart-item.css";
import { ReactComponent as DeleteIcon } from "./delete-material-icon.svg";

export const CartItem = ({ item }) => {
  const { handleDeleteAllItemsByTypeFromBasket } = useShoppingContext();
  const { basketItems } = useShopping();

  const quantity = basketItems[item.id].count;
  const totalPriceByItem = (quantity * item.price).toFixed(2);

  return (
    <div className="cart_item">
      <img src={item.img} alt={item.name} />
      <div className="info">
        <h2>{item.name}</h2>
        <h3>{`$${item.price}`}</h3>
        <div className="cta_container">
          <ActionButtons item={item} />
          <DeleteIcon
            onClick={() => handleDeleteAllItemsByTypeFromBasket(item.id)}
          />
        </div>
      </div>
      <h3 className="total_price">Total: {`$${totalPriceByItem}`}</h3>
    </div>
  );
};
