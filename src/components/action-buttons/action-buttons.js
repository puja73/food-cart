import { useShoppingContext } from "../shopping/shopping.context";
import { useShopping } from "../shopping/shopping.hook";
import "./action-buttons.css";

export const ActionButtons = ({ item }) => {
  const { basketItems } = useShopping();
  const { handleAddItemToBasket, handleRemoveItemFromBasket } =
    useShoppingContext();

  const quantity = basketItems[item.id].count;

  return (
    <div className="action_buttons_container">
      <button onClick={() => handleRemoveItemFromBasket(item.id)}>-</button>
      {quantity}
      <button onClick={() => handleAddItemToBasket(item)}>+</button>
    </div>
  );
};
