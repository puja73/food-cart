export const truncate = (str, length) => {
  return str.slice(0, length) + "...";
};
