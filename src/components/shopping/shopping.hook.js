import { useShoppingContext } from "./shopping.context";

export const useShopping = () => {
  const { basket } = useShoppingContext();

  let basketTotalPrice = 0;

  const basketItems = basket.reduce((acc, cur) => {
    if (acc[cur.id]) {
      acc[cur.id].count++;
    } else {
      acc[cur.id] = {
        count: 1,
      };
    }

    basketTotalPrice += cur.price;

    return acc;
  }, {});

  const basketItemsLength = Object.keys(basketItems).length;

  return {
    basketItems,
    basketItemsLength,
    basketTotalPrice,
  };
};
