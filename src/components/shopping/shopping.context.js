import React, { useContext } from "react";

export const ShoppingContext = React.createContext();

export const useShoppingContext = () => {
  const context = useContext(ShoppingContext);

  if (!context) {
    throw Error("Context should be provided");
  }

  return context;
};
