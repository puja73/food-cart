import React, { useState } from "react";
import { Cart } from "../cart/cart";
import { Menu } from "../menu/menu";
import { ShoppingContext } from "./shopping.context";
import "./shopping.css";

export const Shopping = () => {
  const [basket, setBasket] = useState([]);

  const handleAddItemToBasket = item => setBasket(prev => [...prev, item]);

  const handleRemoveItemFromBasket = itemId => {
    // itemId to be removed from the basket
    const itemIdx = basket.findIndex(i => i.id === itemId);

    // if item is found, make a new temp basket, remove from that and update the basket
    if (itemIdx > -1) {
      const currentItems = [...basket];
      currentItems.splice(itemIdx, 1);
      setBasket(currentItems);
    }
  };

  const handleDeleteAllItemsByTypeFromBasket = itemId =>
    setBasket(prev => prev.filter(p => p.id !== itemId));

  const context = {
    basket,
    handleAddItemToBasket,
    handleRemoveItemFromBasket,
    handleDeleteAllItemsByTypeFromBasket,
  };

  return (
    <ShoppingContext.Provider value={context}>
      <div className="shopping_container">
        <Menu />
        <Cart />
      </div>
    </ShoppingContext.Provider>
  );
};
