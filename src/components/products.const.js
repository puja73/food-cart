export const PRODUCTS = [
  {
    id: "FF",
    img: "https://images.unsplash.com/photo-1639744091981-2f826321fae6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8ZnJlbmNoJTIwZnJpZXN8ZW58MHwyfDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60",
    name: "French fries with Ketchup",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!",
    price: 2.23,
    count: 0,
  },
  {
    id: "SV",
    img: "https://images.unsplash.com/photo-1602881917445-0b1ba001addf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8c2FsbW9ufGVufDB8MnwwfHw%3D&auto=format&fit=crop&w=500&q=60",
    name: "Salmon and Vegetables",
    price: 5.12,
    description:
      "Lorem ipsum dolor sit amet consecteLorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!dipisicing elit. Explicabo placeat debitis, rem in, consequatur dignissimos ut hic inventore atque illum quas! Perferendis repellat quidem cupiditate nesciunt fuga sit nulla numquam odio optio minus, ut deserunt veritatis!",
    count: 0,
  },
  {
    id: "SM",
    img: "https://images.unsplash.com/photo-1595295333158-4742f28fbd85?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8c3BhZ2hldHRpJTIwaW4lMjBtZWF0JTIwc2F1Y2V8ZW58MHwyfDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60",
    name: "Spaghetti Meat Sauce",
    price: 7.82,
    description:
      "Lorem ipsum dolor sit amet consecteLorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!milique, error aspernatur? Laboriosam minima error, culpa maiores, expedita tempore cumque corrupti eum, amet saepe perspiciatis.",
    count: 0,
  },
  {
    id: "BE",
    img: "https://images.unsplash.com/photo-1559067933-0293effe6133?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjV8fGJhY29uJTIwdG9hc3R8ZW58MHwyfDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60",
    name: "Bacon, Eggs and Toast",
    price: 17.86,
    description:
      "Lorem ipsum dolor sLorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, itaque!. Possimus eum hic dignissimos nisi dolores fuga.",
    count: 0,
  },
];
